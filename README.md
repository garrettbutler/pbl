# PBL

PBL repository for CGDs project.

## Description
This Gitlab repository contains the code and pipeline to run clustering models. Based on internal, transactional, and industry ESG data the model aims to cluster clients according to how sustainable they are.

## Installation

**Code environment:**

Clone this repository on your local device. We assume by now you have installed a type of conda. The following steps will allow the user to perform the clustering models provided in Pipeline.ipynb:

1. Open Anaconda prompt ("terminal" if you work on MAC)
2. In your prompt, locate the cloned repository folder.
3. Enter the following line of code to create a conda environment and ensure that all required packages are installed:

- conda env create --name PBL --file PBL.yaml

4. Run the environment and open jupyterlab

Either via Anaconda Navigator or via prompt, code:

conda activate PBL
jupyter-lab (jupyter lab on MAC)

**Data:**

To successfully run the code, the proxy dataset and data related to card movements (parquet files) need to be uploaded to the following directories:

Data directory:
- proxy data set (filename: "Final_proxy.xlsx")

Parquet directory (within data directory):
- all parquet files concerning card_movements (e.g. filename: "card_movements_20210930_000000.parquet")


## Usage
To successfully run the model the following steps should be followed:

1) Install all required packages using the yaml file as described under 'Installation'
2) Upload data to the corresponding folders as described under 'Installation'
3) Launch JuppyterLab or JupyterNotebook and open the 'Pipeline.ipynb' file
4) Run cell by cell sequentially

User Input:
- The user is required to enter their MySQL Database username and password to establish a connection.
- The user has the option of choosing their preferred model, either KMeans or KMedoids, as well as the number of cluster (n_clusters) by calling the model_choice() function.
- The user has the option of choosing specific or all features by calling the feature_choice() function.
- The user has the option of identifying the cluster of a specific client by calling the find_cluster() function.

Note: All functions are documented with docstrings explaining what the function does and all the necessary input parameters. In jupyter lab the docstrings can be accessed by hitting shift + tab. 

## Support
For any support please contact:

- Isabel Maria Mora Labarca - 48516@novasbe.pt
- Mariana Alexandra Freitas Filipe - 39552@novasbe.pt
- Tomás Henrique Rijo Mendes - 48429@novasbe.pt
- Felix Kocher - 50308@novasbe.pt
- Garrett Andrew Butler - 48763@novasbe.pt


## Authors and acknowledgment

- Isabel Maria Mora Labarca - 48516@novasbe.pt
- Mariana Alexandra Freitas Filipe - 39552@novasbe.pt
- Tomás Henrique Rijo Mendes - 48429@novasbe.pt
- Felix Kocher - 50308@novasbe.pt
- Garrett Andrew Butler - 48763@novasbe.pt

## License

Project is equipped with an MIT License.

## Project status

Project is complete and has been handed over to CGD. 
