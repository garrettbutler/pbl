import pandas as pd
import numpy as np
import os
import matplotlib.pyplot as plt
import seaborn as sns
import mysql.connector as mysql
import warnings
warnings.filterwarnings('ignore')
from sklearn.cluster import KMeans
from sklearn.metrics import silhouette_samples, silhouette_score
import matplotlib.cm as cm
from sklearn.preprocessing import StandardScaler
from sklearn_extra.cluster import KMedoids
import time
from sklearn.impute import SimpleImputer
from yellowbrick.cluster import KElbowVisualizer
from sklearn.decomposition import PCA



def database_connection():
    """
    Function that enables the user to connect to CGD's database. The user is required to enter their username and password. 

    Parameters
    -----------
    username (input): string
        username required to connect to CGD's database.

    password: string
        password required to connect to CGD's database. 
    
    Returns
    ---------------
    db: CMySQLConnection (object)
        database connection is used as an input to load data.
    """
    username = input('What is your usename?: ')
    password = input('What is your password?: ')
    db = mysql.connect(
        host = "172.20.20.4",
        user = username,
        passwd = password,
        database = "cgd")
    print("Successfully connected to database")
    return db
    

def load_infosir_data(database_connection):
    """
    Function that loads in selected data from the infosir2021 table. 

    Parameters
    -----------
    database_connection: CMySQLConnection (object)
        database_connection is required to access and extract the data from CGD's database. It is inputted as the con parameter in the pandas.read_sql() statement. 
    
    Returns
    ---------------
    infosir_df: dataframe
        dataframe containing selected data from infosir2021.
    """
    
    infosir_df = pd.read_sql('SELECT n_cliente, n_contribuinte, c_ae_ajtd, c_pais_isoa_sede,\
                              c_sect_ittl_bpor, c_sit_emp, n_gecn, z_cstz_emp, i_consolidado,\
                              c_notz_ratg, c_segm_risc_ratg, cp_tot, custo_mercadorias_vendidas_e_materias_consumidas,\
                              f_prob_icpt, gastos_com_pessoal, invent_act_bio, net_debt, pass_tot,\
                              pc_fornecedores, pnc_resp_beneficios_pos_emprego, rec_ciclicos, rend_operacionais,\
                              res_liq_periodo, res_operacional_antes_gastos_financiamento_e_imp, subs_a_exploracao, tot_act FROM infosir2021', con = database_connection)
    return infosir_df


def load_leasing_contracts_data(database_connection):
    """
    Function that loads in selected data from the leasing_contracts table. 

    Parameters
    -----------
    database_connection: CMySQLConnection (object)
        database_connection is required to access and extract the data from CGD's database. It is inputted as the con parameter in the pandas.read_sql() statement. 
    
    Returns
    ---------------
    leasing_contracts_df: dataframe
        dataframe containing selected data from leasing_contracts.
    """
    leasing_contracts_df = pd.read_sql('SELECT nif, numero_contrato FROM leasing_contracts', con = database_connection)
    return leasing_contracts_df


def load_leasing_assets_data(database_connection):
    """
    Function that loads in selected data from the leasing_assets table. 

    Parameters
    -----------
    database_connection: CMySQLConnection (object)
        database_connection is required to access and extract the data from CGD's database. It is inputted as the con parameter in the pandas.read_sql() statement. 
    
    Returns
    ---------------
    leasing_assets_df: dataframe
        dataframe containing selected data from leasing_assets.
    """
    leasing_assets_df = pd.read_sql('SELECT numero_contrato, bem, estado_bem FROM leasing_assets', con = database_connection)
    return leasing_assets_df


def loading_confirming_data(database_connection):
    """
    Function that loads in selected data from the confirming_entities table. 

    Parameters
    -----------
    database_connection: CMySQLConnection (object)
        database_connection is required to access and extract the data from CGD's database. It is inputted as the con parameter in the pandas.read_sql() statement. 
    
    Returns
    ---------------
    confirming_entities_df: dataframe
        dataframe containing selected data from confirming_entities.
    """
    confirming_entities_df = pd.read_sql('SELECT nif, tipo_intervencao, cae, desc_cae, forma_juridica, distrito, n_empregados FROM confirming_entities', con = database_connection)
    return confirming_entities_df


def loading_non_sepa_issued_data(database_connection):
    """
    Function that loads in selected data from the non_sepa_issued_international_transf table. 

    Parameters
    -----------
    database_connection: CMySQLConnection (object)
        database_connection is required to access and extract the data from CGD's database. It is inputted as the con parameter in the pandas.read_sql() statement. 
    
    Returns
    ---------------
    non_sepa_issued: dataframe
        dataframe containing selected data from non_sepa_issued_international_transf.
    """
    non_sepa_issued = pd.read_sql('SELECT n_cliente, numero_transferencias_internacionais FROM non_sepa_issued_international_transf', con = database_connection)
    return non_sepa_issued


def loading_non_sepa_received_data(database_connection):
    """
    Function that loads in selected data from the non_sepa_received_international_transf table. 

    Parameters
    -----------
    database_connection: CMySQLConnection (object)
        database_connection is required to access and extract the data from CGD's database. It is inputted as the con parameter in the pandas.read_sql() statement. 
    
    Returns
    ---------------
    non_sepa_received: dataframe
        dataframe containing selected data from non_sepa_received_international_transf.
    """
    non_sepa_received = pd.read_sql('SELECT n_cliente, numero_transferencias_internacionais FROM non_sepa_received_international_transf', con = database_connection)
    return non_sepa_received


def card_movements_data(filepath):
    """
    Function that loads in data concerning card movements from parquet files. 

    Parameters
    -----------
    filepath: str
        filepath of the folder that contains all the parquet files related to card movements.  
    
    Returns
    ---------------
    card_movements_df: dataframe
        dataframe containing data related to card movements.
    """
    card_movements_df = pd.read_parquet(filepath, engine = 'auto')
    return card_movements_df


def loading_proxy_data_set(filepath):
    """
    Function that loads in the proxy dataset containing ESG related metrics. 

    Parameters
    -----------
    filepath: str
        filepath of the folder that contains all the proxy dataset.  
    
    Returns
    ---------------
    proxy_df: dataframe
        dataframe containing ESG related data.
    """
    proxy_df = pd.read_excel(filepath)
    return proxy_df


def preprocessing_infosir(infosir_df):
    """
    Function that preprocesses the infosir dataframe. 

    Parameters
    -----------
    infosir_df: dataframe
        dataframe containing data from the inforsir table.  
    
    Returns
    ---------------
    infosir_df: dataframe
        preprocessed infosir dataframe. 
    """
    infosir_df.drop(infosir_df[infosir_df['n_contribuinte'] == '73f84b8774884c0daf3d031c63236bbc'].index, inplace = True)

    # Drop the repeated n_cliente - One number repeats 85789 times + 3 others appear twice
    infosir_df.drop_duplicates(subset = 'n_cliente', keep = False, inplace = True)
    
    # Renaming "n_contribuinte" for merging purposes
    infosir_df.rename(columns={"n_contribuinte":"nif"}, inplace = True)
    
    #Creating CAEs
    infosir_df['c_ae_ajtd'] = infosir_df['c_ae_ajtd'].astype('string')
    infosir_df.rename(columns={"c_ae_ajtd":"cae"}, inplace = True)
    
    # Adjusting incorrect CAE Codes
    infosir_df.drop(infosir_df[infosir_df['cae'].str.len() > 5].index, inplace = True)
    infosir_df['cae'].loc[(infosir_df['cae'].str.len() == 4) & (infosir_df['cae'].str.startswith('1413')) & (infosir_df['cae'].str.startswith('1091')) & (infosir_df['cae'].str.startswith('1013')) & (infosir_df['cae'].str.startswith('1102')) & (infosir_df['cae'].str.startswith('1085')) & (infosir_df['cae'].str.startswith('1621')) & (infosir_df['cae'].str.startswith('1623')) & (infosir_df['cae'].str.startswith('1011'))] = infosir_df['cae'].astype(str) + '0' 
    infosir_df['cae'].loc[(infosir_df['cae'].str.len() == 4) & (infosir_df['cae'].str.startswith('1'))] = '0' + infosir_df['cae'].astype(str)
    infosir_df.loc[(infosir_df['cae'].str.len() == 4) & (infosir_df['cae'].str.startswith('2100')) & (infosir_df['cae'].str.startswith('2400')) & (infosir_df['cae'].str.startswith('2200')) & (infosir_df['cae'].str.startswith('2300'))] = '0' + infosir_df['cae'].astype(str)
    infosir_df['cae'].loc[(infosir_df['cae'].str.len() == 4) & (infosir_df['cae'].str.startswith('2'))] = infosir_df['cae'].astype(str) + '0'
    infosir_df.loc[(infosir_df['cae'].str.len() == 4) & (infosir_df['cae'].str.startswith('3111')) & (infosir_df['cae'].str.startswith('3210'))]  = '0' + infosir_df['cae'].astype(str)
    infosir_df['cae'].loc[(infosir_df['cae'].str.len() == 4) & (infosir_df['cae'].str.startswith('3'))] = infosir_df['cae'].astype(str) + '0'
    infosir_df['cae'].loc[(infosir_df['cae'].str.len() == 4) & (infosir_df['cae'].str.startswith('4'))] = infosir_df['cae'].astype(str) + '0'
    infosir_df['cae'].loc[(infosir_df['cae'].str.len() == 4) & (infosir_df['cae'].str.startswith('5'))] = infosir_df['cae'].astype(str) + '0'
    infosir_df['cae'].loc[(infosir_df['cae'].str.len() == 4) & (infosir_df['cae'].str.startswith('6'))] = infosir_df['cae'].astype(str) + '0'
    infosir_df['cae'].loc[(infosir_df['cae'].str.len() == 4) & (infosir_df['cae'].str.startswith('7290'))] = '0' + infosir_df['cae'].astype(str)
    infosir_df['cae'].loc[(infosir_df['cae'].str.len() == 4) & (infosir_df['cae'].str.startswith('7'))] = infosir_df['cae'].astype(str) + '0'
    infosir_df['cae'].loc[(infosir_df['cae'].str.len() == 4) & (infosir_df['cae'].str.startswith('8292')) & (infosir_df['cae'].str.startswith('8130'))] = infosir_df['cae'].astype(str) + '0'
    infosir_df['cae'].loc[(infosir_df['cae'].str.len() == 4) & (infosir_df['cae'].str.startswith('8'))] = '0' + infosir_df['cae'].astype(str)
    infosir_df['cae'].loc[(infosir_df['cae'].str.len() == 4) & (infosir_df['cae'].str.startswith('9'))] = infosir_df['cae'].astype(str) + '0'
    infosir_df['cae'].loc[(infosir_df['cae'].str.len() == 3)] = '0' + infosir_df['cae'].astype(str) + '0'
    infosir_df['cae'].replace('0', np.nan, inplace=True)
    
    #Grouping CAEs using the first 2 digits
    infosir_df['2 Digits'] = infosir_df['cae'].str[:-3]
    grouping_lists = [['01', '02', '03'], ['05','06','07', '08', '09'], ['10','11','12','13','14','15','16','17','18',
                                                                     '19','20','21','22','23','24','25','26','27','28','29','30','31',
                                                                    '32','33'], ['35'], ['36','37','38','39'], ['41','42','43'], ['45','46','47'],
                                                                    ['49','50','51','52','53'], ['55','56'], ['58','59','60','61','62','63'],
                                                                      ['64','65','66'], ['68'], ['69','70','71','72','73','74','75'], ['77','78','79','80','81','82'],
                                                                      ['84'], ['85'], ['86', '87', '88'], ['90', '91', '92', '93'], ['94', '95', '96'],
                                                                      ['97', '98'], ['99']]
    bins = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U']
    maps = (pd.DataFrame({'Section': bins, '2 Digits': grouping_lists})
            .explode('2 Digits')
            .reset_index(drop=True))
    infosir_df = infosir_df.merge(maps, on = '2 Digits', how = 'left').fillna("NA")
    
    # Adjusting date column
    infosir_df['z_cstz_emp'].replace('00010101_000000', np.nan, inplace=True)
    infosir_df['date_new'] = infosir_df['z_cstz_emp'].str[:8]
    infosir_df["date_new"] = pd.to_numeric(infosir_df["date_new"])
    infosir_df["date_new"] = pd.to_datetime(infosir_df['date_new'], format = "%Y%m%d", errors = "coerce")
    infosir_df["Company_Years"] = 2022 - pd.DatetimeIndex(infosir_df['date_new']).year
    
    #Creating Dummy data regarding Foreign or National
    infosir_df['c_pais_isoa_sede'].replace(to_replace = '', value = np.nan, inplace = True)
    infosir_df.loc[infosir_df['c_pais_isoa_sede'] == 'pt', 'c_pais_isoa_sede_new'] = 'National' 
    infosir_df.loc[infosir_df['c_pais_isoa_sede'] != 'pt', 'c_pais_isoa_sede_new'] = 'Foreign'
    infosir_df = pd.get_dummies(infosir_df, columns = ['c_pais_isoa_sede_new'], prefix='Origin')
    infosir_df.drop(columns = ['c_pais_isoa_sede'], inplace = True)
    
    #Company Situation
    infosir_df['c_sit_emp'].replace(to_replace = 'NA', value = np.nan, inplace = True)
    numbers = [[1,17], [2], [10, 11], [12, 13, 14], [3,4,9,21]]
    text = ['In progress', 'Registered', 'Integrated', 'Dissolved', 'Other']
    maps_6 = (pd.DataFrame({'c_sit_emp': numbers, 'c_sit_emp_bins': text})
            .explode('c_sit_emp')
            .reset_index(drop=True))
    infosir_df = infosir_df.merge(maps_6, on = 'c_sit_emp', how = 'left').fillna("NA")
    infosir_df['c_sit_emp_bins'].replace(to_replace = 'NA', value = np.nan, inplace = True)
    infosir_df = pd.get_dummies(infosir_df, columns = ['c_sit_emp_bins'], prefix='Situation')
    infosir_df.drop(columns = ['c_sit_emp'], inplace = True)

    # Adding CAE descriptions
    letters = [['A'], ['B'], ['C'], ['D'], ['E'], ['F'], ['G'], ['H'], ['I'], ['J'], ['K'], ['L'], ['M'], ['N'], ['O'], ['P'], ['Q'], ['R'], ['S'], ['T'], ['U']]
    text = ['Agricultura, produção animal, caça, floresta e pesca','Indústrias extractivas',' Indústrias transformadoras','Electricidade, gás, vapor, água quente e fria e ar frio','Captação, tratamento e distribuição de água; saneamento, gestão de resíduos e despoluição','Construção','Comércio por grosso e a retalho; reparação de veículos automóveis e motociclos',' Transportes e armazenagem','Alojamento, restauração e similares','Actividades de informação e de comunicação','Actividades financeiras e de seguros','Actividades imobiliárias','Actividades de consultoria, científicas, técnicas e similares','Actividades administrativas e dos serviços de apoio','Administração Pública e Defesa; Segurança Social Obrigatória','Educação','Actividades de saúde humana e apoio social','Actividades artísticas, de espectáculos, desportivas e recreativas','Outras actividades de serviços','Actividades das famílias empregadoras de pessoal doméstico e actividades de produção das famílias para uso próprio','Actividades dos organismos internacionais e outras instituições extra-territoriais']
    maps_2 = (pd.DataFrame({'Section': letters, 'Section Name': text})
            .explode('Section')
            .reset_index(drop=True))
    infosir_df = infosir_df.merge(maps_2, on = 'Section', how = 'left').fillna("NA")
    PT = ['Agricultura, produção animal, caça, floresta e pesca','Indústrias extractivas',' Indústrias transformadoras','Electricidade, gás, vapor, água quente e fria e ar frio','Captação, tratamento e distribuição de água; saneamento, gestão de resíduos e despoluição','Construção','Comércio por grosso e a retalho; reparação de veículos automóveis e motociclos',' Transportes e armazenagem','Alojamento, restauração e similares','Actividades de informação e de comunicação','Actividades financeiras e de seguros','Actividades imobiliárias','Actividades de consultoria, científicas, técnicas e similares','Actividades administrativas e dos serviços de apoio','Administração Pública e Defesa; Segurança Social Obrigatória','Educação','Actividades de saúde humana e apoio social','Actividades artísticas, de espectáculos, desportivas e recreativas','Outras actividades de serviços','Actividades das famílias empregadoras de pessoal doméstico e actividades de produção das famílias para uso próprio','Actividades dos organismos internacionais e outras instituições extra-territoriais']
    EN = ['Agriculture, animal production, hunting, forestry and fishing', 'Extractive industries', 'Manufacturing industries', 'Electricity, gas, steam, hot and cold water and cold air', 'Collection, treatment and distribution of water, sanitation, waste management and depollution', 'Construction', 'Wholesale and retail trade, vehicle repair cars and motorcycles', 'Transportation and storage', 'Accommodation, food service industry and similar', 'Information and communication activities', 'Financial and insurance activities', 'Real estate activities', 'Consulting, scientific, technical and similar activities', 'Administrative activities and support services', 'Public Administration and Defense, Mandatory Social Security', 'Education', 'Human health activities and social support', 'Artistic, entertainment, sporting and recreational activities', 'Other service activities', 'Activities of households employing domestic staff and family production activities for their own use', 'Activities of international organizations and other extraterritorial institutions']
    maps_3 = (pd.DataFrame({'Section Name': PT, 'EN': EN})
            .explode('EN')
            .reset_index(drop=True))
    infosir_df = infosir_df.merge(maps_3, on = 'Section Name', how = 'left').fillna("NA")
    infosir_df.drop(columns = ['z_cstz_emp', '2 Digits', 'Section Name', 'cae', 'c_sect_ittl_bpor'], inplace = True)
    
    # Company Consolidation
    infosir_df['i_consolidado'].replace(to_replace = 'g', value = 1, inplace = True)
    infosir_df['i_consolidado'].replace(to_replace = 'i', value = 0, inplace = True)
    
    # Ratings
    infosir_df['c_notz_ratg'].replace(to_replace = '', value = np.nan, inplace = True)
    infosir_df['c_notz_ratg'].replace(to_replace = 'bom', value = 3, inplace = True)
    infosir_df['c_notz_ratg'].replace(to_replace = 'satf', value = 6, inplace = True)
    infosir_df['c_notz_ratg'].replace(to_replace = 'frco', value = 9, inplace = True)
    infosir_df['c_notz_ratg'].replace(to_replace = 'frte', value = 1, inplace = True)
    infosir_df['c_notz_ratg'].replace(to_replace = 'd', value = 15, inplace = True)
    infosir_df.drop(columns = ['c_segm_risc_ratg'], inplace = True)
    return infosir_df


def leasing_contracts_processing(leasing_contracts):
    """
    Function that preprocesses the leasing_contracts dataframe. 

    Parameters
    -----------
    leasing_contracts: dataframe
        dataframe containing data from the leasing_contracts table.  
    
    Returns
    ---------------
    leasing_contracts: dataframe
        preprocessed leasing_contracts dataframe.
    """
    leasing_contracts.drop_duplicates(subset = 'numero_contrato', keep = False, inplace = True)
    return leasing_contracts
    

    
def leasing_assets_processing(leasing_assets):
    """
    Function that preprocesses the leasing_assets dataframe. 

    Parameters
    -----------
    leasing_assets: dataframe
        dataframe containing data from the leasing_assets table.  
    
    Returns
    ---------------
    leasing_assets: dataframe
        preprocessed leasing_assets dataframe.
    """
    grouping_lists = [['HABITACAO', 'A.COMERCIAL', 'E.COMERCIAL', 'ARM/ESCRITORIO', 'A.INDUSTRIAL', 'ESCRITORIO', 'RESTAURANTE','OTR COM/ADM','STAND/GAR/OFIC','COMERCIAL/ADM', 'E.INDUSTRIAL', 'HOTEL', 'ARMAZENAGEM MATERIAL', 'HAB SECUNDARIA', 'MOBILIARIO','INDUST. USO ESPECIF'], ['INST SOCIAL'], ['TERRENO'], ['PESADOS E REBOQUES', 'Aviões', 'Navios', 'Barcos', 'LIGEIROS E MISTOS', 'PESADOS PASSAGEIROS', 'GRUAS E BARCAÇAS','Embarc. Fibra Vidro', 'ATRELADOS, EMPILH', 'OUTROS VEÍCULOS', 'Tractores','MAQ SUJ MATRICULA','MOTORES'], ['EQUIPAMENTO AGRICOLA'], ['INDUST. TABACO'], ['INFORMÀTICO', 'ELECTRÓNICA'], ['METALÚRGICA', 'CERÂMICA E VIDRO', 'MISTO INDUSTRIAL', 'ESCAVAÇÃO','EXPLOR. PEDREIRAS'], ['CURTUMES','TEXTIL E CALÇADO','ESPEC TRANSFORMAÇÃO'], ['CONSTRUÇÃO ESTRADAS', 'ANDAIMES E COFRAGENS','ESPEC CONSTR CIVIL'],  ['MÁQUINAS IMPRESSÃO', 'GRÁFICA'], ['ENERGIA SOLAR'], ['OUTRO', 'EQUIPAMENTO DIVERSO', 'UTENSÍLIOS COZINHA','LAVAGEM AUTOMÁTICA', 'MÉDICO E LABORATORIO','DESENHO TOPOGRAFIA','EQUIPAMENTO OFICINAS','OUTROS','MOVIM. E DESCARGA', 'CONSERV/CONGELAÇÃO', 'AQUEC. VENTILAÇÃO', 'NAO CLASSIFICADO']]
    bins = ['Buildings, offices and storage','Social institutions','Land','Transportation', 'Agricultural equipment','Tobacco industry','Eletronic equipment','Extractive industry','Manufacturing industry','Construction','Printing equipment', 'Solar energy', 'Other equipment']
    maps_4 = (pd.DataFrame({'bem': grouping_lists, 'Category of leased asset': bins})
            .explode('bem')
            .reset_index(drop=True))
    leasing_assets = leasing_assets.merge(maps_4, on = 'bem', how = 'left')
    leasing_assets = leasing_assets[['numero_contrato','bem','Category of leased asset','estado_bem']]
    leasing_assets = pd.get_dummies(leasing_assets, columns = ['Category of leased asset'], prefix='Leasing')
    leasing_assets.replace(to_replace = '', value = np.nan, inplace = True)
    leasing_assets = pd.get_dummies(leasing_assets, columns = ['estado_bem'], prefix = 'Leasing')
    leasing_assets = leasing_assets.groupby('numero_contrato').agg('sum').reset_index()
    return leasing_assets


def non_sepa_issued_processing(non_sepa_issued):
    """
    Function that preprocesses the non_sepa_issued dataframe. 

    Parameters
    -----------
    non_sepa_issued: dataframe
        dataframe containing data from the non_sepa_issued table.  
    
    Returns
    ---------------
    non_sepa_issued: dataframe
        preprocessed non_sepa_issued dataframe.
    """
    non_sepa_issued.rename(columns={"numero_transferencias_internacionais":"numero_transferencias_internacionais_issued"}, inplace = True)
    non_sepa_issued = non_sepa_issued.groupby('n_cliente').agg('sum').reset_index()
    return non_sepa_issued


def non_sepa_received_processing(non_sepa_received):
    """
    Function that preprocesses the non_sepa_received dataframe. 

    Parameters
    -----------
    non_sepa_received: dataframe
        dataframe containing data from the non_sepa_received table.  
    
    Returns
    ---------------
    non_sepa_received: dataframe
        preprocessed non_sepa_received dataframe.
    """
    non_sepa_received.rename(columns={"numero_transferencias_internacionais":"numero_transferencias_internacionais_received"}, inplace = True)
    non_sepa_received = non_sepa_received.groupby('n_cliente').agg('sum').reset_index()
    return non_sepa_received
    

def card_movements_processing(card_movements):
    """
    Function that preprocesses the card_movements dataframe. 

    Parameters
    -----------
    card_movements: dataframe
        dataframe containing data from the card_movements table.  
    
    Returns
    ---------------
    card_movements: dataframe
        preprocessed card_movements dataframe.
    """
    card_movements.drop(columns = ['c_pan','ts_trmz_sibs', 'c_trnz_epdd_sibs', 'd_trnz_epdd_sibs', 'c_tipo_term_sibs', 'i_cart_dbcr', 'i_vrtt_cart', 'i_estn_sibs', 'c_mcc_sibs', 'c_term_sibs', 'n_ctbe_cmct_sibs', 'c_dist_clho_sibs', 'nm_concelho', 'nm_distrito', 'x_lclz_term_sibs'], inplace = True)
    card_movements = card_movements[card_movements['i_sinl_trnz_sibs'] == 'D'].copy()
    card_movements.drop(columns = 'i_sinl_trnz_sibs', inplace = True)
    card_movements['c_cae'] = card_movements['c_cae'].astype('string')
    card_movements.rename(columns={"c_cae":"cae"}, inplace = True)
    card_movements['cae'].loc[(card_movements['cae'].str.len() == 4) & (card_movements['cae'].str.startswith('1413')) & (card_movements['cae'].str.startswith('1091')) & (card_movements['cae'].str.startswith('1013')) & (card_movements['cae'].str.startswith('1102')) & (card_movements['cae'].str.startswith('1085')) & (card_movements['cae'].str.startswith('1621')) & (card_movements['cae'].str.startswith('1623')) & (card_movements['cae'].str.startswith('1011'))] = card_movements['cae'].astype(str) + '0'
    card_movements['cae'].loc[(card_movements['cae'].str.len() == 4) & (card_movements['cae'].str.startswith('1'))] = '0' + card_movements['cae'].astype(str) 
    card_movements.loc[(card_movements['cae'].str.len() == 4) & (card_movements['cae'].str.startswith('2100')) & (card_movements['cae'].str.startswith('2400')) & (card_movements['cae'].str.startswith('2200')) & (card_movements['cae'].str.startswith('2300'))] = '0' + card_movements['cae'].astype(str)
    card_movements['cae'].loc[(card_movements['cae'].str.len() == 4) & (card_movements['cae'].str.startswith('2'))] = card_movements['cae'].astype(str) + '0'
    card_movements.loc[(card_movements['cae'].str.len() == 4) & (card_movements['cae'].str.startswith('3111')) & (card_movements['cae'].str.startswith('3210'))]  = '0' + card_movements['cae'].astype(str)
    card_movements['cae'].loc[(card_movements['cae'].str.len() == 4) & (card_movements['cae'].str.startswith('3'))] = card_movements['cae'].astype(str) + '0'
    card_movements['cae'].loc[(card_movements['cae'].str.len() == 4) & (card_movements['cae'].str.startswith('4'))] = card_movements['cae'].astype(str) + '0'
    card_movements['cae'].loc[(card_movements['cae'].str.len() == 4) & (card_movements['cae'].str.startswith('5'))] = card_movements['cae'].astype(str) + '0'
    card_movements['cae'].loc[(card_movements['cae'].str.len() == 4) & (card_movements['cae'].str.startswith('6'))] = card_movements['cae'].astype(str) + '0'
    card_movements['cae'].loc[(card_movements['cae'].str.len() == 4) & (card_movements['cae'].str.startswith('7290'))] = '0' + card_movements['cae'].astype(str)
    card_movements['cae'].loc[(card_movements['cae'].str.len() == 4) & (card_movements['cae'].str.startswith('7'))] = card_movements['cae'].astype(str) + '0' 
    card_movements['cae'].loc[(card_movements['cae'].str.len() == 4) & (card_movements['cae'].str.startswith('8292')) & (card_movements['cae'].str.startswith('8130'))] = card_movements['cae'].astype(str) + '0'
    card_movements['cae'].loc[(card_movements['cae'].str.len() == 4) & (card_movements['cae'].str.startswith('8'))] = '0' + card_movements['cae'].astype(str)
    card_movements['cae'].loc[(card_movements['cae'].str.len() == 4) & (card_movements['cae'].str.startswith('9'))] = card_movements['cae'].astype(str) + '0' 
    card_movements['cae'].loc[(card_movements['cae'].str.len() == 3)] = '0' + card_movements['cae'].astype(str) + '0'
    card_movements['cae'].replace('0', np.nan, inplace=True)
    card_movements['2 Digits (beneficiary)'] = card_movements['cae'].str[:-3]
    
    grouping_lists = [['01', '02', '03'], ['05','06','07', '08', '09'], ['10','11','12','13','14','15','16','17','18',
                                                                     '19','20','21','22','23','24','25','26','27','28','29','30','31',
                                                                    '32','33'], ['35'], ['36','37','38','39'], ['41','42','43'], ['45','46','47'],
                                                                    ['49','50','51','52','53'], ['55','56'], ['58','59','60','61','62','63'],
                                                                      ['64','65','66'], ['68'], ['69','70','71','72','73','74','75'], ['77','78','79','80','81','82'],
                                                                      ['84'], ['85'], ['86', '87', '88'], ['90', '91', '92', '93'], ['94', '95', '96'],
                                                                      ['97', '98'], ['99']]
    
    bins = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U']
    maps = (pd.DataFrame({'Section (beneficiary)': bins, '2 Digits (beneficiary)': grouping_lists})
            .explode('2 Digits (beneficiary)')
            .reset_index(drop=True))
    card_movements = card_movements.merge(maps, on = '2 Digits (beneficiary)', how = 'left').fillna("NA")
    card_movements['Section (beneficiary)'].replace(to_replace = 'NA', value = np.nan, inplace = True) 
    card_movements = pd.pivot_table(card_movements, values = 'm_trnz_sibs', index = 'n_cliente', columns = 'Section (beneficiary)', aggfunc = np.sum)
    card_movements['total_expenditure'] = card_movements.sum(axis = 1)
    return card_movements

def proxy_proceesing(proxy_df):
    """
    Function that preprocesses the proxy_df dataframe. 

    Parameters
    -----------
    proxy_df: dataframe
        dataframe containing data from the proxy table.  
    
    Returns
    ---------------
    proxy_df: dataframe
        preprocessed proxy dataframe.
    """
    proxy_df['Greenhouse gases'] = proxy_df['Greenhouse gases'].astype(str).str.replace(' ','').astype(float)
    proxy_df['Air Emissions Intensities; Greenhouse gases'] = proxy_df['Air Emissions Intensities; Greenhouse gases'].str.replace(' ','').astype(float)
    proxy_df['Carbon intensity of the economy'] = proxy_df['Carbon intensity of the economy'].str.replace(' ','').astype(float)
    proxy_df['Monthly average salaries'] = proxy_df['Monthly average salaries'].str.replace(' ','').astype(float)
    return proxy_df


def merging_dataframes(infosir_df_processed, leasing_contracts_processed, leasing_assets_processed, confirming_entities, non_sepa_issued_processed, non_sepa_received_processed, card_movements_processed, proxy_df_processed):
    """
    Function that merges all dataframes. 

    Parameters
    -----------
    infosir_df_processed: dataframe
        preprocessed infosir dataframe.  
    
    leasing_contracts_processed: dataframe
        preprocessed leasing_contracts dataframe.
        
    leasing_assets_processed: dataframe
        preprocessed leasing_assets dataframe.
        
    confirming_entities: dataframe
        confirming_entities dataframe.
        
    non_sepa_issued_processed: dataframe
        preprocessed non_sepa_issued dataframe.
        
    non_sepa_received_processed: dataframe
        preprocessed non_sepa_received dataframe.
        
    card_movements_processed: dataframe
        preprocessed card_movements dataframe.
        
    proxy_df_processed: dataframe
        preprocessed proxy dataframe. 
    
    Returns
    ---------------
    df: dataframe
        a merged dataframe containing data from all tables.
    """
    leasing = pd.merge(leasing_contracts_processed, leasing_assets_processed, how = 'left', on = 'numero_contrato')
    leasing = leasing.groupby('nif').agg('sum').reset_index()
    df = pd.merge(infosir_df_processed, leasing, how = 'left', on = 'nif')
    clients = confirming_entities[confirming_entities['tipo_intervencao'] == 'cliente']
    clients = clients.drop_duplicates(subset = 'nif', keep = False)
    clients.drop(columns = ['tipo_intervencao', 'cae', 'desc_cae', 'forma_juridica', 'distrito'], inplace = True)
    df = pd.merge(df, clients, how = 'left', on = 'nif')
    df = pd.merge(df, non_sepa_issued_processed, how = 'left', on = 'n_cliente')
    df = pd.merge(df, non_sepa_received_processed, how = 'left', on = 'n_cliente') 
    df = pd.merge(df, card_movements_processed, how = 'left', on = 'n_cliente')
    df = pd.merge(df, proxy_df_processed, left_on='Section', right_on='Sector')
    
    dataFileName = os.path.join("data", "AllDataMerged.csv")
    df.to_csv(dataFileName)
    return df


def dropcols(df, columns):
    """
    Function that drops columns from a dataframe. 

    Parameters
    -----------
    df: dataframe
        dataframe from which columns should be dropped.
    
    columns: list
        list of strings, containing the names of columns to be dropped.
    
    Returns
    ---------------
    df: dataframe
        dataframe that does not contain the dropped columns anymore.
    """
    df.drop(columns = columns, inplace= True)
    return df


def replacing_zeros(df, columns):
    """
    Function that replaces zeroes with NaN values in certain columns of a dataframe. 

    Parameters
    -----------
    df: dataframe
        dataframe in which zeroes should be replaced with NaN values.
    
    columns: list
        list of strings, containing the names of columns in which zeroes should be replaced with NaN values.
    
    Returns
    ---------------
    df: dataframe
        dataframe in which zeroes have been replaced with NaN values for given columns.
    """
    df[columns] = df[columns].replace(0, np.nan)
    return df


def replacing_nans(df, columns):
    """
    Function that replaces NaN values with zeroes in certain columns of a dataframe. 

    Parameters
    -----------
    df: dataframe
        dataframe in which NaN values should be replaced with zeroes.
    
    columns: list
        list of strings, containing the names of columns in which NaN values should be replaced with zeroes.
    
    Returns
    ---------------
    df: dataframe
        dataframe in which NaN values have been replaced with zeroes for given columns.
    """
    df[columns] = df[columns].replace(np.nan, 0)
    return df
    
    

def checking_missing_values(df, columns):
    """
    Function that displays the percentage of missing values in certain columns of a dataframe. 

    Parameters
    -----------
    df: dataframe
        dataframe for which missing values should be checked.
    
    columns: list
        list of strings, containing the names of columns for which missing values should be checked.
    
    Returns
    ---------------
    perc_df: dataframe
        dataframe in which shows the column and the percentage of missing values.
    """
    perc = df[columns].isnull().sum(axis = 0)/ (df[columns].isnull().sum(axis = 0) + df[columns].count())*100
    perc_df = pd.DataFrame(perc)
    perc_df = perc_df[0].sort_values(ascending = False)

    return perc_df


def dropping_cols_mv_threshold(df, columns, threshold):
    """
    Function that drops columns of a dataframe that have a percentage of missing values above a certain threshold. 

    Parameters
    -----------
    df: dataframe
        dataframe for which certain columns should be dropped.
    
    columns: list
        list of strings, containing the names of columns to be dropped.
        
    threshold: float
        percentage threshold, for which columns with missing values above this threshold are dropped. Range between 0-1.
    
    Returns
    ---------------
    df: dataframe
        dataframe that does not contain the dropped columns anymore.
    """
    col_perc = checking_missing_values(df, columns)
    a = np.array(col_perc[col_perc > threshold].index)
    df.drop(columns = a, inplace= True)
    return df


def replace_negatives(df, cols):
    """
    Function that replaces negative values with NaN values in certain columns of a dataframe. 

    Parameters
    -----------
    df: dataframe
        dataframe in which negative values should be replaced with NaN values.
    
    cols: list
        list of strings, containing the names of columns in which negative values should be replaced with NaN values.
    
    Returns
    ---------------
    df: dataframe
        dataframe in which negative values have been replaced with NaN values for given columns.
    """
    df[cols].values[df[cols] < 0] = np.nan
    return df


def dropping_rows_mv_threshold(df, columns, threshold):
    """
    Function that drops rows of a dataframe that have a missing values in percentage of columns above a certain threshold. (E.g. if threshold = 0.5, rows with missing values in more than 50% of the columns are dropped.)

    Parameters
    -----------
    df: dataframe
        dataframe for which certain columns should be dropped.
    
    columns: list
        list of strings, containing the names of columns to be dropped.
        
    threshold: float
        percentage threshold, for which the number of columns contain missing values. Range between 0-1.
    
    Returns
    ---------------
    df: dataframe
        dataframe that does not contain the dropped rows anymore.
    """
    thresh = threshold
    df['null_count'] = (df[columns].isnull().sum(axis=1)/len(df[columns].columns))
    df = df[df['null_count'] < thresh]
    df.drop(columns= 'null_count',inplace= True)
    return df


def IQR(df, cols):
    """
    Function that applies the 1.5 IQR method to handle outliers. IQR = Q3 - Q1 (with Q1 and Q3 being the 25th and 75th percentile respectively). 
    Lower Bound: Q1 - 1.5 * IQR
    Upper Bound: (Q3 + 1.5 * IQR)
    All instances below the lower bound / above the upper bound are replaced by the closest in-range value

    Parameters
    -----------
    df: dataframe
        dataframe for which columns should should have the IQR method applied to. 
    
    columns: list
        list of strings, containing the names of numerical columns that should have the IQR method applied to.
  
    Returns
    ---------------
    df: dataframe
        dataframe that that has had IQR method applied to given columns.
    """
    for col in cols:
        Q3 = df[col].quantile(q =0.75)
        Q1 = df[col].quantile(q = 0.25)
        A = Q3-Q1
        UB = Q3 + 1.5*A
        LB = Q1 - 1.5*A
        df[col].values[df[col] > UB] = UB
        df[col].values[df[col] < LB] = LB
    return df


def median_imputation(df, cols):
    """
    Function that imputes NaN values with the median value of companies in similar industries.
    Parameters
    -----------
    df: dataframe
        dataframe in which NaNs should be imputed by the median.  
    
    cols: list
        list of strings containing the names of numerical columns that should have NaN values imputed by the median.
  
    Returns
    ---------------
    df: dataframe
        dataframe for wich NaN values were imputed by the median for given columns.
    """
    for col in cols:
        df[col] = df[col].fillna(df.groupby('Section')[col].transform('median'))   
    return df


def remove_industry_based(dataset):
    """
    Function that removes industry-based columns (client number and industry).
    Parameters
    -----------
    dataset: dataframe
        dataframe for which columns related to client number and industry should be dropped.  
    
    Returns
    ---------------
    df: dataframe
        dataframe without columns related to client number and industry.
    """
    columns = ['n_cliente','Section']
    df = dataset.copy()
    for col in columns:
        if col in dataset.columns:
            df.drop(columns=col, inplace=True)
    return df


def remove_zeros_nulls(dataset):
    """
    Function that filters the dataset for columns with less than 95% zeros and less than 30% missing values.  
    Parameters
    -----------
    dataset: dataframe
        dataframe for which columns should be cleaned for null values and zeros.  
    
    Returns
    ---------------
    df: dataframe
        dataframe without columns that have more than 95% zeros and more than 30% missing values.
    """
    good_feats = []
    for i in dataset.columns:
        if (((dataset[i] == 0).sum()/dataset.shape[0]) <= 0.95) and (dataset[i].isnull().sum()/dataset.shape[0]) <= 0.3 :
            good_feats.append(i)
    df = dataset[good_feats]
    return df


def scale_and_impute(dataset):
    """
    Function that scales and imputes data. Data is scaled using sklearns StandardScaler(). Missing values are imputed with the median using sklearns SimpleImputer().
    Parameters
    -----------
    dataset: dataframe
        dataframe for which data should be scaled and imputed.  
    
    Returns
    ---------------
    df: dataframe
        dataframe containing scaled and imputed data.
    """
    imp = SimpleImputer(missing_values=np.nan, strategy='median')
    features_imputed = imp.fit_transform(dataset)
    sc = StandardScaler()
    features_scaled = sc.fit_transform(features_imputed)
    features_scaled_df = pd.DataFrame(features_scaled, index=dataset.index, columns=dataset.columns)
    return features_scaled_df


def create_features(cleaned_df):
    """
    Function that transforms features to create additional ESG metrics to be implemented in the model.
    The focus of this exercise is to design features that hold relevant ESG insights.
    Every unnecessary column will be added to the list of 'dropcols', leter to be dropped.
    The reason for having 'n_cliente' in the index is to avoid inconsistencies in "n_cliente" as some entries exist multiple times.
    Every data frame captures at least one feature:
    1. Probability_of_Default
        - Given in data set
        - Unrealistic values are replaced with max/min allowance out; only values between [0,1] deemed realistic
    2. gross_profit_margin
        - Financial parameter to about company efficiency formed by EBIT/Revenue
        - Unrealistic values are replaced with max/min allowance out; only values between [-1,1] deemed realistic
    3. Debt_to_Equity_Ratio
        - Unrealistic values are replaced with max/min allowance out; only values between [0,2] deemed realistic
    4. Labor_Cost_per_Employee
        - Total labor costs divided by aprox no. of employees
        - Cleaned with IQR
    5. Wage_Compared_to_Industry_Average
        - As the "employee" counts are missing for almost all companies, this function predicts the number of employees.
        - Total personnel expenses per year are divided by 14 months of salary (with bonuses) and divided by the average salary per month.
        - Compares company's personnel expenses per euro of revenue with its industry average
        - Cleaned with IQR
    6. GHG_Emissions_Multiple_Gases_per_EUR_Revenue
        - An industry proxy is multiplied by revenue to estimate the number of accidents.
        - Cleaned with IQR
    7. CO2_Scope_1+2
        - An industry average proxy gets multiplied with the company size (revenue over average revenue of industry)
        - Cleaned with IQR
    8. Accounts_Payable_to_Suppliers_per_EUR_Revenue
        - How much money the company ows to suppliers per EUR of revenue
        - Cleaned with IQR

    Parameters
    -----------
    final_features_df: dataframe
        The data frame that comprises all relevant features that are yet to be transformed.

    Returns
    ---------------
    final_features_df: dataframe
        dataframe with 8 additional created features.
    """
    
    final_features_df = cleaned_df[[
        'n_cliente', 'f_prob_icpt', 'pass_tot', "cp_tot", 'Monthly average salaries',
        'gastos_com_pessoal', 'res_operacional_antes_gastos_financiamento_e_imp',
        'rend_operacionais', 'Section', 'Greenhouse gases', 'pc_fornecedores', 'D',
    ]]
    
    dropcols = []
    technical_cleaning = []

    #Approximate Nr. of employees
    final_features_df['approx_employees'] = abs(final_features_df['gastos_com_pessoal']) / 14 / final_features_df['Monthly average salaries']
    dropcols.extend(['Monthly average salaries'])
    
    # Probability of default
    final_features_df['f_prob_icpt'] = np.where(final_features_df['f_prob_icpt'] < 0, 0, np.where(final_features_df['f_prob_icpt'] > 1, 1, final_features_df['f_prob_icpt'])) 
    final_features_df.rename(columns = {'f_prob_icpt':'Probability_of_Default'}, inplace = True)
    
    # Financial Ratio about company efficiency: Gross Profit Margin
    final_features_df['gross_profit_margin'] = final_features_df['res_operacional_antes_gastos_financiamento_e_imp'] / final_features_df['rend_operacionais']
    final_features_df['gross_profit_margin'] = np.where(final_features_df['gross_profit_margin'] < -1, -1, np.where(final_features_df['gross_profit_margin'] > 1, 1, final_features_df['gross_profit_margin'])) 
    dropcols.extend(['res_operacional_antes_gastos_financiamento_e_imp'])
    
    # Debt-Equity ratio
    final_features_df['debt_to_equity'] = final_features_df["pass_tot"] / final_features_df["cp_tot"]
    final_features_df['debt_to_equity'] = np.where(final_features_df['debt_to_equity'] < 0, 0, np.where(final_features_df['debt_to_equity'] > 2, 2, final_features_df['debt_to_equity'])) 
    final_features_df.rename(columns = {'debt_to_equity':'Debt_to_Equity_Ratio'}, inplace = True)
    dropcols.extend(["pass_tot", "cp_tot"])
    
    # Labor_Cost_per_Employee
    final_features_df["Labor_Cost_per_Employee"] = (final_features_df["gastos_com_pessoal"] / final_features_df["approx_employees"]).abs()
    technical_cleaning.extend(["Labor_Cost_per_Employee"])
    dropcols.extend(["gastos_com_pessoal", "approx_employees"])
    
    # Wage_Compared_to_Industry_Average
    final_features_df["Labor_Cost_per_Revenue"] = (final_features_df["gastos_com_pessoal"] / final_features_df["rend_operacionais"]).abs()
    labor_costs_by_revenue_avg_per_section = final_features_df.groupby("Section")["Labor_Cost_per_Revenue"].agg("mean").to_frame()
    labor_costs_by_revenue_avg_per_section.rename(columns = {'Labor_Cost_per_Revenue':'Labor Cost/revenue_by_industry'}, inplace = True)
    final_features_df = final_features_df.merge(labor_costs_by_revenue_avg_per_section, on="Section", how = "left")
    final_features_df["Wage_Compared_to_Industry_Average"] = final_features_df["Labor_Cost_per_Revenue"] / final_features_df["Labor Cost/revenue_by_industry"]  
    technical_cleaning.extend(["Wage_Compared_to_Industry_Average"])
    dropcols.extend(['Labor_Cost_per_Revenue', "Labor Cost/revenue_by_industry"])
    
    # GHG_Emissions_Multiple_Gases_per_EUR_Revenue
    final_features_df['GHG_Emissions_Multiple_Gases_per_EUR_Revenue'] = final_features_df['Greenhouse gases'] / final_features_df['rend_operacionais']
    technical_cleaning.extend(["GHG_Emissions_Multiple_Gases_per_EUR_Revenue"])
    dropcols.extend(['Greenhouse gases'])
    
    # CO2 Scope 1+2
    df_average_spendings_per_industry = final_features_df[['Section', 'D']]
    df_average_spendings_per_industry = df_average_spendings_per_industry.groupby('Section').mean() 
    df_average_spendings_per_industry.rename(columns = {'D':'avg_D'}, inplace= True)
    final_features_df = final_features_df.merge(df_average_spendings_per_industry, how='left',on='Section')
    final_features_df['CO2_Scope_1+2'] = (final_features_df['D']/final_features_df['avg_D'])*final_features_df['Greenhouse gases']
    technical_cleaning.extend(["CO2_Scope_1+2"])
    dropcols.extend(['D', 'Greenhouse gases', 'avg_D'])
    
    # Accounts_Payable_to_Suppliers_per_EUR_Revenue
    final_features_df["Accounts_Payable_to_Suppliers_per_EUR_Revenue"] = (final_features_df["pc_fornecedores"] / final_features_df["rend_operacionais"]).abs()
    technical_cleaning.extend(["Accounts_Payable_to_Suppliers_per_EUR_Revenue"])
    dropcols.extend(['pc_fornecedores'])
    
    # Keep Identifier!
    # No changes to variables: ['n_cliente']
    
    # Clean All IQRs based on technical Cleaning
    final_features_df = IQR(final_features_df, technical_cleaning)

    
    # Dropcols. Make set unique. Then dropcols.
    dropcols_set = set(dropcols)
    dropcols = list(dropcols_set)
    final_features_df.drop(dropcols, axis = 1, inplace = True)
    
    # Groupby n_cliente to make all n_cliente values equal (use mean if several exist). Then reset index.
    # final_features_df = final_features_df.groupby(['n_cliente']).mean().reset_index()
    return final_features_df


def model_choice():
    """
    Function that lets the user chose between k-means or k-medoids for modelling. The user also has the option to adjust hyperparameters (number of clusters: n_clusters).
    If the user does not choose to adjust the hyperparamters, the model will run with the optimized hyperparameters".
    The function also tests for valid entries.

    Parameters (Inputs)
    -------------------
    choice_1: string
        The chosen model.
    
    choice_2: string
        Whether the user wishes to adjust the hyperparameters.
    
    user_n_clusters: integer
        The number of n_clusters the user wishes to use for the model. If the user does not wish to adjust hyperparamters, it is set to the optimized number.

    Returns
    ---------------
    final_model: list
        List containing:
        Position[0] - choice_1 i.e. name of model (either k-means or k-medoids)
        Position[1] - the clustering model i.e. KMeans(..) or KMedoids(...)
        Position[2] - user_n_clusters i.e. the chosen number of clusters
    """
    stop_1 = False
    while stop_1 is not True:
        print('''Wich model should be utilized?
              - 'K-Means' 
              - 'K-Medoids' 
              - To leave, type 'exit'.''')
        global choice_1
        choice_1 = input("Please input your choice here: ").lower()
        if choice_1 == 'k-means':
            choice_2 = input("Would you like to adjust hyperparameters manually? ('yes' or 'no')").lower()
            if choice_2 == 'no':
                user_n_clusters = 3
                clustering_model = KMeans(n_clusters=user_n_clusters, random_state=42)
            else:
                user_n_clusters = int(input("Please enter the number of clusters as an integer: "))
                clustering_model = KMeans(n_clusters=user_n_clusters, random_state=42)
            stop_1 = True
        elif choice_1 == 'k-medoids':
            choice_2 = input("Would you like to adjust hyperparameters manually? ('yes' or 'no')").lower()
            if choice_2 == 'no':
                user_n_clusters = 2
                clustering_model = KMedoids(n_clusters=user_n_clusters, random_state=42)
            else:
                user_n_clusters = int(input("Please enter the number of clusters as an integer: "))
                clustering_model = KMedoids(n_clusters=user_n_clusters, random_state=42)
            stop_1 = True
        elif choice_1 == 'exit':
            print("The application will shut down.")
            break
        else:
            print("Please chose a valid entry!")
        final_model = [choice_1, clustering_model, user_n_clusters]
    return  final_model


def feature_choice(final_model, df):
    """
    Function that lets the user chose whether all or only seleceted features should be used for the model. If not all features are selected,  the user can input the features one by one.
    Due to limited computational resources, the function checks If the inputed model is K-medoids, if so a sample of the data is taken.  The function also tests for valid entries.

    Parameters & Inputs
    --------------------
    final_model: list
        The chosen model.
        
    df: dataframe
        dataframe that contains the features.

    choice_2: string
        'yes' if the user wants to use all possible features, else 'no'.
    
    choice_3: string
        feature name. 
    
    Returns
    ---------------
    df: dataframe
        returns the dataframe with all features. If a k-medoids model was chosen, the dataframe will be a sample of the original dataframe. 
    
    selected_features: dataframe
       scaled and imputed dataframe containing only the chosen features. 
    """
    print("Attached, you will find a list of all possible features.")
    available_features = ['Probability_of_Default', 'rend_operacionais', 'gross_profit_margin', 'Debt_to_Equity_Ratio', 'Wage_Compared_to_Industry_Average', 'GHG_Emissions_Multiple_Gases_per_EUR_Revenue', 'Accounts_Payable_to_Suppliers_per_EUR_Revenue']
    print(available_features)
    stop_2 = False
    while stop_2 is not True:
        choice_2 = input("Do you like to evaluate all features ('yes'/'no'): ").lower()
        selected_features = []
        if choice_2 == 'yes':
            stop_2 = True
            selected_features = list(df.columns)
        elif choice_2 == 'no':
            stop_3 = False
            while stop_3 is not True:
                choice_3 = input("Please input all features that you would like to evaluate without the ''. When you are done, enter 'exit': ")
                if choice_3 != 'exit':
                    if choice_3 in list(df.columns):
                        selected_features.append(choice_3)
                        print("So far, you have chosen:", selected_features)
                    else:
                        print("Please enter a valid entry!")
                else:
                    stop_2 = True
                    stop_3 = True
                    print("Thank you, you have selected the following features:\n", selected_features)                
        else:
            print("Please enter a valid entry ('yes' or 'no')!")  
    if final_model[0] == 'k-means':
        df = df[selected_features]
        selected_features = scale_and_impute(remove_zeros_nulls(remove_industry_based(df)))
        return df, selected_features
    else:
        df = df.sample(frac=0.07, random_state = 42)
        selected_features = scale_and_impute(remove_zeros_nulls(remove_industry_based(df)))
        return df, selected_features
    
def kmedoids_this(final_model, clustering_model, df, features_table, new_filename, plot_results=True):
    """
    Function that executes a k-medoids clustering model. 
    If plot_results is set to 'True', the function will visualize the clustering results in a 2 dimensional space using pca and the silhouette score. 
    
    Parameters & Inputs
    --------------------
    final_model: list
        The chosen model.
    
     clustering_model: KMedoids object
        the clustering model i.e. KMedoids(...).
        
    df: dataframe
        dataframe that contains all the features.

    features_table: dataframe
        dataframe that only contains the chosen features.
    
    new_filename: string
         name of the file where the clustering results should be saved to. 
    
    plot_results: boolean
        True if results should be plotted, else False.     
         
    Returns
    ---------------
    kmedoids_try: dataframe
        returns a dataframe with all features as well as the clustering results in the 'label' column. 
    """
    X = features_table.dropna()
    n_clusters = final_model[2]
    cluster_labels = clustering_model.fit_predict(X)
    print(f"Model for {n_clusters} clusters already ran!")

    silhouette_avg = silhouette_score(X, cluster_labels)
    print(
    "For n_clusters =",
    n_clusters,
    "The average silhouette_score is :",
    silhouette_avg,
    )
    # Labeling the clusters
    centers = clustering_model.cluster_centers_

    kmedoids_try = df[X.columns]
    kmedoids_try['labels'] = cluster_labels
    kmedoids_try['n_cliente'] = df['n_cliente']

    dataFileName = os.path.join("data", new_filename +'.csv')
    kmedoids_try.to_csv(dataFileName)

    porps = kmedoids_try['labels'].value_counts(normalize=True)
    print(porps)
    
    if plot_results==True:
        # Create a subplot with 1 row and 2 columns
        fig, (ax1, ax2) = plt.subplots(1, 2)
        fig.set_size_inches(18, 7)

        # The 1st subplot is the silhouette plot
        # The silhouette coefficient can range from -1, 1 but in this example all
        ax1.set_xlim([-0.1, 1])
        # The (n_clusters+1)*10 is for inserting blank space between silhouette
        # plots of individual clusters, to demarcate them clearly.
        ax1.set_ylim([0, len(X) + (n_clusters + 1) * 10])

        # Compute the silhouette scores for each sample
        sample_silhouette_values = silhouette_samples(X, cluster_labels)

        y_lower = 10
        for i in range(n_clusters):
            # Aggregate the silhouette scores for samples belonging to
            # cluster i, and sort them
            ith_cluster_silhouette_values = sample_silhouette_values[cluster_labels == i]

            ith_cluster_silhouette_values.sort()

            size_cluster_i = ith_cluster_silhouette_values.shape[0]
            y_upper = y_lower + size_cluster_i

            color = cm.nipy_spectral(float(i) / n_clusters)
            ax1.fill_betweenx(
                np.arange(y_lower, y_upper),
                0,
                ith_cluster_silhouette_values,
                facecolor=color,
                edgecolor=color,
                alpha=0.7,
            )

            # Label the silhouette plots with their cluster numbers at the middle
            ax1.text(-0.05, y_lower + 0.5 * size_cluster_i, str(i))

            # Compute the new y_lower for next plot
            y_lower = y_upper + 10  # 10 for the 0 samples

        ax1.set_title("The silhouette plot for the various clusters.")
        ax1.set_xlabel("The silhouette coefficient values")
        ax1.set_ylabel("Cluster label")

        # The vertical line for average silhouette score of all the values
        ax1.axvline(x=silhouette_avg, color="red", linestyle="--")

        ax1.set_yticks([])  # Clear the yaxis labels / ticks
        ax1.set_xticks([-0.1, 0, 0.2, 0.4, 0.6, 0.8, 1])

        pca = PCA(n_components=2, random_state = 42)
        pca.fit(X)
        data = pca.transform(X)

        pca_try = pd.DataFrame(data)

        pca_try = pca_try.rename(columns = {0 : "Feature 1", 1: "Feature 2"})
        pca_try['labels'] = cluster_labels

        #plot data
        ax2 = sns.scatterplot(data = pca_try, x = "Feature 1", y ="Feature 2", hue = "labels").set(title='Clustering with Features Transformed by PCA')
        plt.show()
   
    return kmedoids_try


def kmeans_this(final_model, clustering_model, df, features_table, new_filename, plot_results=True):
    """
    Function that executes a KMeans clustering model. 
    If plot_results is set to 'True', the function will visualize the clustering results in a 2 dimensional space using pca and the silhouette score. 
    
    Parameters & Inputs
    --------------------
    final_model: list
        The chosen model.
    
     clustering_model: KMeans object
        the clustering model i.e. KMeans(...).
        
    df: dataframe
        dataframe that contains all the features.

    features_table: dataframe
        dataframe that only contains the chosen features.
    
    new_filename: string
         name of the file where the clustering results should be saved to. 
    
    plot_results: boolean
        True if results should be plotted, else False.     
         
    Returns
    ---------------
    kmeans_try: dataframe
        returns a dataframe with all features as well as the clustering results in the 'label' column. 
    """
    
    X = features_table

    # Initialize the clusterer with n_clusters value and a random generator
    cluster_labels = clustering_model.fit_predict(X)

    # The silhouette_score gives the average value for all the samples.This gives a perspective into the density and separation of the formed
    silhouette_avg = silhouette_score(X, cluster_labels)
    n_clusters = final_model[2]
    print("For n_clusters =",n_clusters,"The average silhouette_score is :", silhouette_avg)

    kmeans_try = df[features_table.columns]
    kmeans_try['labels'] = cluster_labels
    kmeans_try['n_cliente'] = df['n_cliente']

    dataFileName = os.path.join("data", new_filename +'.csv')
    kmeans_try.to_csv(dataFileName)
    
    cluster_proportions(kmeans_try)
    
    if plot_results==True:
        
        # Create a subplot with 1 row and 2 columns
        fig, (ax1, ax2) = plt.subplots(1, 2)
        fig.set_size_inches(18, 7)

        # The 1st subplot is the silhouette plot
        # The silhouette coefficient can range from -1, 1 but in this example all
        ax1.set_xlim([-0.1, 1])
        # The (n_clusters+1)*10 is for inserting blank space between silhouette
        # plots of individual clusters, to demarcate them clearly.
        ax1.set_ylim([0, len(X) + (n_clusters + 1) * 10])

        # Compute the silhouette scores for each sample
        sample_silhouette_values = silhouette_samples(X, cluster_labels)

        y_lower = 10
        for i in range(n_clusters):
            # Aggregate the silhouette scores for samples belonging to
            # cluster i, and sort them
            ith_cluster_silhouette_values = sample_silhouette_values[cluster_labels == i]

            ith_cluster_silhouette_values.sort()

            size_cluster_i = ith_cluster_silhouette_values.shape[0]
            y_upper = y_lower + size_cluster_i

            color = cm.nipy_spectral(float(i) / n_clusters)
            ax1.fill_betweenx(
                np.arange(y_lower, y_upper),
                0,
                ith_cluster_silhouette_values,
                facecolor=color,
                edgecolor=color,
                alpha=0.7,
            )

            # Label the silhouette plots with their cluster numbers at the middle
            ax1.text(-0.05, y_lower + 0.5 * size_cluster_i, str(i))

            # Compute the new y_lower for next plot
            y_lower = y_upper + 10  # 10 for the 0 samples

        ax1.set_title("The silhouette plot for the various clusters.")
        ax1.set_xlabel("The silhouette coefficient values")
        ax1.set_ylabel("Cluster label")

        # The vertical line for average silhouette score of all the values
        ax1.axvline(x=silhouette_avg, color="red", linestyle="--")

        ax1.set_yticks([])  # Clear the yaxis labels / ticks
        ax1.set_xticks([-0.1, 0, 0.2, 0.4, 0.6, 0.8, 1])

        pca = PCA(n_components=2, random_state = 42)
        pca.fit(X)
        data = pca.transform(X)

        pca_try = pd.DataFrame(data)

        pca_try = pca_try.rename(columns = {0 : "Feature 1", 1: "Feature 2"})
        pca_try['labels'] = cluster_labels

        #plot data
        ax2 = sns.scatterplot(data = pca_try, x = "Feature 1", y ="Feature 2", hue = "labels").set(title='Clustering with Features Transformed by PCA')
        plt.show()
    return kmeans_try


def cluster_proportions(result_table):
    """
    Function that executes a KMeans clustering model. 
    If plot_results is set to 'True', the function will visualize the clustering results in a 2 dimensional space using pca and the silhouette score. 
    
    Parameters & Inputs
    --------------------
    result_table: dataframe
        dataframe that contains the results of the clustering model (output of kmeans_this() / kmedoids_this() function).
         
    Returns
    ---------------
    porps: string
        prints the porpotions of companies allocated to each cluster. 
    """
    porps =result_table['labels'].value_counts(normalize=True)
    print(porps)


def elbow_this(model, features):
    """
    Function that optimizes for the number of clusters using the Elbow method. 
    The clustering model runs with a different number of clusters (between 2 and 8) and plots the silhouette scores.
    
    Parameters
    --------------------
    model: KMeans() or KMedoids() object
        The model for which n_cluster should be optimized for
       
    Returns
    ---------------
    visualizer: plot
        plot displaying a silhouette score for each model iteration with a different number of clusters.  
    """
    if model=='k-means':
        model = KMeans()
    else:
        model = KMedoids()
    visualizer = KElbowVisualizer(model, k=(2,8), metric= 'silhouette')
    visualizer.fit(features)    
    visualizer.poof()   
    

def run_model(final_model, df, features_table, new_filename, plot_results=True):
    """
    Function that optimizes for the number of clusters using the Elbow method. 
    The clustering model runs with a different number of clusters (between 2 and 8) and plots the silhouette scores.
    
    Parameters
    --------------------
    final_model: list
        the chosen model. Output of model_choice().
        
    features_table: dataframe
        dataframe that only contains the chosen features.
    
    new_filename: string
         name of the file where the clustering results should be saved to. 
    
    plot_results: boolean
        True if results should be plotted, else False.
  
    Returns
    ---------------
     kmeans_model_results: dataframe 
        If the clustering model is kmeans, it will return the output of kmeans_try().
        A dataframe with all features as well as the clustering results in the 'label' column.
        
    kmedoids_model_results: plot
        If the clustering model is kmedoids, it will return the output of kmedoids_try().
        A dataframe with all features as well as the clustering results in the 'label' column.
        
    """
    clustering_model = final_model[1]
    if final_model[0] == 'k-means':
        kmeans_model_results = kmeans_this(final_model, clustering_model, df, features_table, new_filename, plot_results)
        return kmeans_model_results
    else:
        kmedoids_model_results = kmedoids_this(final_model, clustering_model, df, features_table, new_filename, plot_results)
        return kmedoids_model_results
    
def find_cluster(results_frame, client):
    """
    Function that optimizes for the number of clusters using the Elbow method. 
    The clustering model runs with a different number of clusters (between 2 and 8) and plots the silhouette scores.
    
    Parameters
    --------------------
    results_frame: dataframe 
        A dataframe with all features as well as the clustering results in the 'label' column. Output of run_model(...).
        
    client: string
        Client number ('n_cliente').
    
    Returns
    ---------------
     cluster: string 
        string indicating to which cluster a client was allocated to. 
  
    """
    if client not in results_frame['n_cliente'].values:
        print('This client number is not valid or is not in the sample data.')
    else:
        cluster = results_frame.loc[(results_frame['n_cliente'] == client)].labels
        print('Client with client number "{fclient}" was assigned to cluster {fcluster}.'.format(fclient = client, fcluster = cluster.values[0]))
